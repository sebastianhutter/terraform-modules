# terraform-modules

## aws
AWS modules (using provider aws)

## azure 
Azure modules (using provider azure)

## gcp
GCP modules (using provider gcp)

## kubernetes
kubernetes modules (using provider kubernetes)