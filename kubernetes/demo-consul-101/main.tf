# have a look at https://learn.hashicorp.com/consul/getting-started-k8s/minikube

provider "kubernetes" {}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}

  # The latest version of Terragrunt (v0.19.0 and above) requires Terraform 0.12.0 or above.
  required_version = ">= 0.12.0"
}

resource "kubernetes_deployment" "counting-service" {
  metadata {
    name = "counting"
    namespace = "${var.namespace}"
  }

    spec {
        replicas = 1

        selector {
            match_labels = {
                app = "counting"
            }
        }
    
        template {
            metadata {
                labels = {
                    app = "counting"
                }
                annotations = {
                    "consul.hashicorp.com/connect-inject" = "true"
                }
            }
        
            spec {
                container {
                    image = "hashicorp/counting-service:0.0.2"
                    name  = "counting"
                    port {
                        container_port = 9001
                        name          = "http"
                    }
                }
                init_container {
                    image = "hashicorp/counting-init:0.0.9"
                    name  = "counting-init"
                    env {
                        name  = "POD_IP"
                        value_from {
                            field_ref {
                                field_path = "status.podIP"
                            }
                        }
                    }
                    env {
                        name  = "HOST_IP"
                        value_from {
                            field_ref {
                                field_path = "status.hostIP"
                            }
                        }
                    }
                }
            }
        }
    }
}

resource "kubernetes_deployment" "dashboard-service" {
  metadata {
    name = "dashboard"
    namespace = "${var.namespace}"
  }

    spec {
        replicas = 1

        selector {
            match_labels = {
                app = "dashboard"
            }
        }
    
        template {
            metadata {
                labels = {
                    app = "dashboard"
                }
                annotations = {
                    "consul.hashicorp.com/connect-inject" = "true"
                    "consul.hashicorp.com/connect-service-upstreams" = "counting:9001"
                }
            }
        
            spec {
                container {
                    image = "hashicorp/dashboard-service:0.0.4"
                    name  = "dashboard"
                    port {
                        container_port = 9002
                        name          = "http"
                    }
                    env {
                        name  = "COUNTING_SERVICE_URL"
                        value = "http://localhost:9001"
                    }
                }
                init_container {
                    image = "hashicorp/dashboard-init:0.0.4"
                    name  = "dashboard-init"
                    env {
                        name  = "POD_IP"
                        value_from {
                            field_ref {
                                field_path = "status.podIP"
                            }
                        }
                    }
                    env {
                        name  = "HOST_IP"
                        value_from {
                            field_ref {
                                field_path = "status.hostIP"
                            }
                        }
                    }
                }
            }
        }
    }
}

resource "kubernetes_service" "dashboard-service" {
  metadata {
    name = "dashboard"
    namespace = "${var.namespace}"
  }
  spec {
    selector = {
      app = "dashboard"
    }
    port {
      # port the loadbalancer listens on
      port        = 80
      # port the target container listens on
      target_port = 9002
    }
    type = "LoadBalancer"

  }
}