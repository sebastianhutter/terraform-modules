provider "kubernetes" {}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}

  # The latest version of Terragrunt (v0.19.0 and above) requires Terraform 0.12.0 or above.
  required_version = ">= 0.12.0"
}

resource "kubernetes_service" "hello-world" {
  metadata {
    name = "hello-world"
  }
  spec {
    selector = {
      app = "hello-world"
    }
    session_affinity = "ClientIP"
    port {
      # port the loadbalancer listens on
      port        = 80
      # port the target container listens on
      target_port = 8080
    }
    type = "LoadBalancer"
  }
}

resource "kubernetes_deployment" "hello-world" {
  metadata {
    name = "hello-world"
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "hello-world"
      }
    }

    template {
      metadata {
        labels = {
          app = "hello-world"
        }
      }

      spec {
        container {
          image = "paulbouwer/hello-kubernetes:1.5"
          name  = "hello-world"
        }
      }
    }
  }
}