variable "consul_version" {
  type = "string"
  default = "v0.10.0"
}

variable "namespace" {
    type = "string"
}

variable "local_clone" {
    type = "string"
    default = "/tmp/consul-helm"
}