provider "helm" {}
provider "null" {}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}

  # The latest version of Terragrunt (v0.19.0 and above) requires Terraform 0.12.0 or above.
  required_version = ">= 0.12.0"
}

# data "helm_repository" "consul" {
#     name = "consul"
#     url  = "https://github.com/hashicorp/consul-helm.git"
# }

resource "null_resource" "git" {
    provisioner "local-exec" {
        command = "cd /tmp && git clone https://github.com/hashicorp/consul-helm.git && cd consul-helm && git checkout ${var.consul_version}"
    }
}

resource "helm_release" "consul" {
  name          = "consul"
  chart         = "/tmp/consul-helm"
  namespace     = "${var.namespace}"

    set {
        name  = "global.datacenter"
        value = "minikube"
    }
    set {
        name  = "ui.service.type"
        value = "NodePort"
    }
    set {
        name  = "connectInject.enabled"
        value = "true"
    }
    set {
        name  = "client.enabled"
        value = "true"
    }
    set {
        name  = "client.grpc"
        value = "true"
    }
    set {
        name  = "server.replicas"
        value = "1"
    }
    set {
        name  = "server.bootstrapExpect"
        value = "1"
    }
    set {
        name  = "server.disruptionBudget.enabled"
        value = "true"
    }
    set {
        name  = "server.disruptionBudget.maxUnavailable"
        value = "0"
    }

  depends_on    = [
      null_resource.git
  ]
}

# resource "kubernetes_service" "consul" {
#   metadata {
#     name = "consul"
#   }
#   spec {
#     selector = {
#       app = "hello-world"
#     }
#     session_affinity = "ClientIP"
#     port {
#       # port the loadbalancer listens on
#       port        = 80
#       # port the target container listens on
#       target_port = 8080
#     }
#     type = "LoadBalancer"
#   }
# }