provider "kubernetes" {}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}

  # The latest version of Terragrunt (v0.19.0 and above) requires Terraform 0.12.0 or above.
  required_version = ">= 0.12.0"
}

resource "kubernetes_namespace" "namespace" {
    metadata {
       name = "${var.name}" 
    }
}

output "name" {
  value = "${var.name}" 
}