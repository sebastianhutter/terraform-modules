#!/bin/bash

#
# little bash script to setup s3 and dynamo for remote state
# you need to have your aws cli configured (either via env vars or profiles) 

##
# get user input
##

projectname="${1}"
[ -z "${projectname}"] && "Please specify a project name" && exit 1
# @TODO: automatically retrieve kms id from account
kmskeyid="${}"
[ -z "${kmskeyid}"] && "Please specify the s3 kms key to use for the s3 bucket" && exit 1


##
# define aws resources
##

# get the account id
account=$(aws sts get-caller-identity | awk '/Account/ {print $2}' | sed 's/[^0-9]*//g')

## s3 bucket for remote states
bucket="fyayc-tfstate-${account}-${projectname}"
bucketencryption=$(
cat <<- EOF
{
    "Rules": [
        {
            "ApplyServerSideEncryptionByDefault": {
                "SSEAlgorithm": "aws:kms"
            }
        }
    ]
}
EOF
)
buckettags="TagSet=[{Key=Project,Value=${projectname}}]"
bucketpublicaccess="BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
# @TODO: Automatically retrieve kms key id
s3kmskey="${kmskeyid}"

## dynamodb for state locking
table="fyayc-tfstate-${account}-${projectname}"
tableattribute="AttributeName=LockID,AttributeType=S"
tablekeyschema="AttributeName=LockID,KeyType=HASH"
tabletroughput="ReadCapacityUnits=5,WriteCapacityUnits=5"
tabletags="Key=Project,Value=${projectname}"

## user and iam policy
user="fyayc-tfstate-${account}-${projectname}"
policyname="fyayc-tfstate-${account}-${projectname}-access"
policydocument=$(
cat <<- EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetBucketVersioning"
            ],
            "Resource": [
                "arn:aws:s3:::${bucket}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::${bucket}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb:DescribeTable",
                "dynamodb:GetItem",
                "dynamodb:PutItem",
                "dynamodb:DeleteItem"
            ],
            "Resource": [
                "arn:aws:dynamodb:${region}:${account}:table/${table}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt"
            ],
            "Resource": [
                "arn:aws:kms:*:${account}:key/${s3kmskey}"
            ]
        }
    ]
}
EOF
)
usertags="Key=Project,Value=${projectname}"

##
# create aws resources
##

# create a new s3 bucket
aws --region $region s3 mb s3://${bucket}
# enable versioning in the bucket so we can restore lost tfstate files easier
aws --region $region s3api put-bucket-versioning --bucket ${bucket} --versioning-configuration Status=Enabled
# enable encryption at rest with the default kms key
aws --region $region s3api put-bucket-encryption --bucket ${bucket} --server-side-encryption-configuration "$bucketencryption"
# set resource tags on bucket
aws --region $region s3api put-bucket-tagging --bucket ${bucket} --tagging "$buckettags"
# ensure no public access
aws --region $region s3api put-public-access-block --bucket ${bucket} --public-access-block-configuration "$bucketpublicaccess"
# @TODO: enable server access logging
# https://docs.aws.amazon.com/AmazonS3/latest/dev/ServerLogs.html#server-access-logging-overview

# create dynamodb table for locking
aws --region $region dynamodb create-table --attribute-definitions ${tableattribute} --table-name ${table} --key-schema ${tablekeyschema} --provisioned-throughput ${tabletroughput} --tags ${tabletags}

# create iam user for terraform access
aws --region $region iam create-user --user-name ${user} --tags ${usertags}
aws --region $region iam put-user-policy --user-name ${user} --policy-name ${policyname} --policy-document "${policydocument}"
# create the access key - please store it safely in 1password
aws --region $region iam create-access-key --user-name ${user}
```
